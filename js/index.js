function activeNav(path) {
    let server = 'color-ai-solution-demo';
    let serverRex = new RegExp(server, 'g');
    let home = '/';
    let homeRex = new RegExp(home, 'g');
    let processDir = 'process';
    let processRex = new RegExp(processDir, 'g');

    path = path.replace(serverRex, '').replace(homeRex, '').replace(processRex, processDir + '-').replace('.html', '');
    path = path === '' ? 'index' : path;

    let element = $(`#${path}`);

    element.closest('.nav-item').addClass('active');
    $(`title`).html(`顏色總體解決方案AI計畫 - ${element.html()}`);
}

$(function () {
    $('.nav-item').removeClass('active');
    activeNav(window.location.pathname);
});